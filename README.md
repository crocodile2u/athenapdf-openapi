# Athenapdf-openapi

Openapi / Swagger schema for [athenapdf converter](https://github.com/arachnys/athenapdf) web-service.

Use it to generate API client libraries in the language of your choice.
